import numpy as np
import matplotlib.pyplot as plt

benches = (
"",
"perlbench ",
"bzip2 ",
"gcc ",
"mcf ",
"milc ",
"namd ",
"gobmk ",
"soplex ",
"povray ",
"hmmer ",
"sjeng ",
"libquant ",
"h264ref ",
"lbm ",
"omnetpp ",
"sphinx3 ",
"best avg ",
""
)
rt_benches = (
"",
"mcf","milc","hmmer","sjeng","libquantum","h264ref","")
#N = 17 # was 17, was 6
N = 17
osMeans = (
0.0,
0.0,
0.0,
-1.43149284254,
-0.363483852929,
-0.0417264310862,
0.0176341880501,
0.840092699884,
0.0136097241479,
-0.295916915635,
-3.12614482843,
-0.539531023034,
0.165181995163,
0.0,
0.0953987551626,
0.0,
-0.306661436911404   # avg
)
osMeans_all = (
0.0,
0.0,
0.0,
-2.45398773006,
-0.433384593877,
-0.0417264310862,
0.00881709402503,
0.0651796060255,
0.0136097241479,
-0.301607625551,
-3.12614482843,
-0.539531023034,
0.165181995163,
0.0,
0.0953987551626,
-0.0343878954608,
 -0.41425678951896305 # avg
)

osRuntimes90 = ( #4 is segfaults
-1.44288308063,-4.21651736047, 0.151419127871, 0.0, -0.0359667672022, -0.751264804347

)
osRuntimesall = ( # 4 is segfault
-0.654266998487,-4.34342994281,0.191039188719,0.0,-0.119357606484,-1.3498598397
)
ind = np.arange(1,N+1)   # the x locations for the groups
width = 0.41       # the width of the bars

fig, ax = plt.subplots()
rects1 = ax.bar(ind, osMeans_all, width, color='r')

o2Means = (0.0,
0.0,
0.0,
0.0,
-0.0456725279744,
-0.0411967660539,
0.0,
0.157259435566,
0.117464553156,
-0.0712470610587,
-1.40891579527,
0.0704473406129,
1.36762497035,
0.0,
0.0703072082036,
0.0,
 0.013504459846236383
)

o2Means_all = (
0.0,
0.0,
0.0,
0.0,
-0.0456725279744,
-0.0411967660539,
0.0,
-0.278391703502,
0.117464553156,
-0.0712470610587,
-1.40891579527,
0.0704473406129,
1.36762497035,
0.0,
0.0703072082036,
0.0,
 -0.013723736345538917
)
o2Runtimes90 = ( # 4 is segfault, 6 is segfault
0.114187692623,0.243108637509,0.0721825010663,0.0,-0.053889859669, 0.0
)

o2Runtimesall = (# 4 is segfault, 6 is segfault
    -0.522396588509,0.339722989965,-0.01193591171,0.0,-0.0439799051884, 0.0
)
rects2 = ax.bar(ind+width, o2Means_all, width, color='b')

# add some text for labels, title and axes ticks
ax.set_ylabel('Code size reduction in %',size=25) # was 20
ax.set_title('Benchmark',size=25) # was 20
ax.set_xticks(np.arange(0,N+2)+width)
ax.set_xticklabels(benches,rotation='vertical')  # rt_benches
plt.tick_params(axis='both',labelsize=22) # was 15
ax.legend( (rects1[0], rects2[0]), ('-Os', '-O2') )

def autolabel(rects):
    # attach some text labels
    artists = []
    for rect in rects:
        height = rect.get_y()
        height_o = 0
        if height >= 0:
            height_o = rect.get_height()
            height = rect.get_height() + 0.05
        else:
            height_o = height
            height = height - 0.15
        t = ''
        if False and height_o == 0.0:  # rmeove false
            t = ax.text(rect.get_x()+rect.get_width()/2, 1.05*height, 'NaN', ha='center', va='bottom',size=17)
        else:
            t = ax.text(rect.get_x()+rect.get_width()/2, 1.05*height, '%1.1f'%height_o, ha='center', va='bottom',size=17)
        artists.append(t)
    return artists

a1 = autolabel(rects1)
a2 = autolabel(rects2)

plt.tight_layout()
#plt.savefig('best_runtime_o2_os_plot_all.pdf')
plt.show()

