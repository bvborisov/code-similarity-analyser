#include "llvm/ADT/Statistic.h"

#include "llvm/Analysis/DominanceFrontier.h"
#include "llvm/Analysis/LoopInfo.h"
#include "llvm/Analysis/LoopInfoImpl.h"
#include "llvm/Analysis/LoopIterator.h"
#include "llvm/Analysis/LoopPass.h"

#include "llvm/IR/Function.h"

#include "llvm/Pass.h"

#include "llvm/Support/raw_ostream.h"
#include "llvm/Transforms/Scalar.h"
#include "llvm/Transforms/Utils/BasicBlockUtils.h"
#include "llvm/Transforms/Utils/CodeExtractor.h"

#include "llvm/Support/CommandLine.h"
#include <map>

using namespace llvm;

#define DEBUG_TYPE "loopstats"

static cl::opt<std::string> bench("bench",
  cl::Hidden, cl::init(""),
  cl::desc("String identifier for the benchmark"));

static unsigned gCount = 0;

template<class T> static  void print_loop_stats(const std::map<unsigned,T>& map, std::string dscr){
  errs() << "# " << dscr << "\n";
  unsigned acc_debug = 0;
  unsigned size = 1;
  errs() << "xs_" << bench << "_" + std::to_string(gCount)  << " = [";
  for(auto& p: map){
    errs() << p.first; 
    if (size != map.size())
      errs() << ", ";
    else
      errs() << "]\n";
    ++size;
  }
  size = 1;
  errs() << "ys_" << bench << "_" + std::to_string(gCount++)  << " = [";
  for(auto& p: map){
    errs() << p.second; 
    if (size != map.size())
      errs() << ", ";
    else
      errs() << "]\n";
    acc_debug += p.second;
    ++size;
  }
  
  errs() << "# acc_debug: " << acc_debug << "\n";
}

namespace {
  class LoopStats : public LoopPass {
  public:
    static char ID; // Pass identification, replacement for typeid
    LoopStats() : 
      LoopPass(ID),
      LoopCounter(0),
      DepthPerLoop(),
      NumBBsPerLoop(),
      NumInstsPerLoop(),
      NumExUsdDefsPerLoop(),
      NumInstsPerExUsDefs(),
      NumBBsPerExUsDefs()
    {};
    ~LoopStats();
    bool doInitialization(Loop *L, LPPassManager &LPM);
    
    bool runOnLoop(Loop *L, LPPassManager &LPM);
    
    bool doFinalization();
    
    void getAnalysisUsage(AnalysisUsage &AU) const;

    void print(llvm::raw_ostream &O, const Module *M) const;
  private:
    unsigned LoopCounter;
    std::map<unsigned,unsigned> DepthPerLoop;
    std::map<unsigned,unsigned> NumBBsPerLoop;
    std::map<unsigned,unsigned> NumInstsPerLoop;
    std::map<unsigned,unsigned> NumExUsdDefsPerLoop;
    std::map<unsigned,double> NumInstsPerExUsDefs;
    std::map<unsigned,double> NumBBsPerExUsDefs;
  };
}

bool LoopStats::doInitialization(Loop *L, LPPassManager &LPM){
 
  return false;
}

bool LoopStats::runOnLoop(Loop *L, LPPassManager &LPM){
  if (L->getLoopDepth() != 0)
    ++LoopCounter; // loops of all depths
  ++DepthPerLoop[L->getLoopDepth()];
  if (L->getLoopDepth() == 1){
    ++NumBBsPerLoop[L->getNumBlocks()];
    unsigned numInsts = 0;
    for(auto& b: L->getBlocks())
      numInsts += b->size();
    ++NumInstsPerLoop[numInsts];
    // get number of values defined outside of loop body but used inside
    DenseSet<Instruction* > defsInLoop;
    DenseSet<Instruction* > instUsesInLoop;
    for(auto bi = L->block_begin(), be = L->block_end(); bi != be; ++bi){
      for(auto ii = (*bi)->begin(), ie = (*bi)->end(); ii != ie; ++ii){
        defsInLoop.insert(ii);
        for(Use &U: ii->operands()){
          Value* v = U.get();
          if (Instruction* inst = dyn_cast<Instruction>(v))
            instUsesInLoop.insert(inst);
        }
      }
    }
    // filter out defs
    for(auto& u: instUsesInLoop){
      if(defsInLoop.find(u) == defsInLoop.end())
        continue;
      instUsesInLoop.erase(u);
    }
    ++NumExUsdDefsPerLoop[instUsesInLoop.size()];
    NumInstsPerExUsDefs[instUsesInLoop.size()] += defsInLoop.size();
    NumBBsPerExUsDefs[instUsesInLoop.size()] += L->getNumBlocks();
  }
  return false;
}
    
bool LoopStats::doFinalization(){
  return false;
}

void LoopStats::getAnalysisUsage(AnalysisUsage &AU) const {
  AU.setPreservesAll();
  AU.addRequired<LoopInfo>();
  AU.addRequired<DominatorTreeWrapperPass>();
  //AU.addRequiredID(BreakCriticalEdgesID);
  //AU.addRequiredID(LoopSimplifyID);
  AU.addRequired<DominanceFrontier>();    
}

void LoopStats::print(llvm::raw_ostream &O, const Module *M) const{
 
}

LoopStats::~LoopStats(){
  // obtain average maps
  for(auto& p: NumInstsPerExUsDefs){
    p.second /= NumExUsdDefsPerLoop[p.first]*1.0;
  }
  for(auto& p: NumBBsPerExUsDefs){
    p.second /= NumExUsdDefsPerLoop[p.first]*1.0;
  }
  // print stuff
  print_loop_stats(DepthPerLoop, "Number of loops per depth:");
  print_loop_stats(NumBBsPerLoop, "Number of loops per number of basic blocks:");
  print_loop_stats(NumInstsPerLoop, "Number of loops per number of instructions:");
  print_loop_stats(NumExUsdDefsPerLoop, "Number of loops per number of externally defined values used inside those loops:");
  print_loop_stats(NumInstsPerExUsDefs, "Avg number of instructions of all loops that have a particular number of externally defined uses:");
  print_loop_stats(NumBBsPerExUsDefs, "Avg number of basic blocks of all loops that have a particular number of externally defined uses:");
}
// Pass Registration
char LoopStats::ID = 0;
static RegisterPass<LoopStats> X("loopstat", "Loop statistics gathering analysis pass");
