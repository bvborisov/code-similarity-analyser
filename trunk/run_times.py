import subprocess
import time
from timeit import default_timer
import itertools
import os
import md5
from multiprocessing import Pool, Process, Manager, Lock
import matplotlib.pyplot as plt
import numpy as np
import sys
from random import shuffle
from scipy.stats import ttest_rel
cpu_int = ['400.perlbench','401.bzip2', '403.gcc', '429.mcf', '445.gobmk', '456.hmmer', '458.sjeng', '462.libquantum', '464.h264ref', '471.omnetpp','473.astar']   #,'483.xalancbmk']
cpu_fp = ['433.milc','444.namd','447.dealll','450.soplex','453.povray','470.lbm','482.sphinx3']
#best_cands = ['462.libquantum','464.h264ref','433.milc','400.perlbench','401.bzip2']

print "HAVE YOU SOURCED?"
build = 'Release+Asserts'
num_failed = 0
def run_bench((bench,opts)):
    # set nice
    # niceness = os.nice(0)
    # os.nice(niceness - 10)
    # unpack
    #print d
    comp_flag = opts[-3:]
    pass_opts = opts[: -3]
    name = bench.split('.')[1]
    print "lol",name,bench, pass_opts, comp_flag
    try:
        # run opt pass
        dir_str =  bench + pass_opts.replace(' ','_') + comp_flag
        dir_hash = "loop_merge_results12/" + md5.new(dir_str).hexdigest()
        direc = subprocess.call("mkdir -v " + dir_hash, shell=True);
        empty_dir = []
        try:
            empty_dir = os.listdir(dir_hash)
        except:
            pass
        if direc != 0 and "improvement" in empty_dir :
            print empty_dir
            print "Dir exists or is full, move on."
            return
        b = subprocess.call("../llvm-3.5/" + build + "/bin/opt -load ../llvm-3.5/" + build + "/lib/SimMerge.so -simmergefunc " + pass_opts +  " -verify benchspec/CPU2006/" + bench + "/run/build_base_amd64-m64-gcc42-nn.0000/" + name + "_ind_ls.bc  1>"+ dir_hash +"/" + name + "_sim.bc 2>" + dir_hash + "/" + name +".log", shell=True)
        if b != 0:
            print "pass failed, move on"
            subprocess.call('echo "pass failed, move on" && tail ' + dir_hash + "/" + name +".log",shell=True)
            subprocess.call("rm " + dir_hash +"/*", shell=True)
            return 
        # run simplifycfg on top
        b = subprocess.call("opt -simplifycfg -verify " + dir_hash + "/" + name + "_sim.bc 1>" + dir_hash +"/" + name + "_sim_cfg.bc ", shell=True)
        # compile executables
        extra = ''
        if name == "perlbench":
            extra =  " -DSPEC_CPU_LP64 -DSPEC_CPU_LINUX_X64 -lm"
        elif name == "libquantum":
            extra =  " -lm"
        elif name == "gobmk":
            extra = " -lm"
        elif name == "hmmer":
            extra = " -lm"
        elif name == "h264ref":
            extra = " -lm"
        elif name == "milc":
            extra = " -DSPEC_CPU_LP64 -lm"
        elif name == "soplex": 
            extra = " -lm"
        elif name == "sphinx3":
            extra = " -lm"
        elif name == "lbm":
            extra = " -lm"
        elif name == "gcc":
            extra = " -lm -DSPEC_CPU_LP64"
        # set compiler
        comp = " clang "
        if name == "omnetpp" or name == "astar" or name == "namd" or name == "soplex" or name == "povray":
            comp = "clang++ "
        subprocess.call(comp + comp_flag + " -o " + dir_hash + "/" + name + "_sim "  + " " + dir_hash + "/" + name+ "_sim_cfg.bc" + extra, shell=True)
        subprocess.call("strip -s " + dir_hash + "/" + name + "_sim", shell=True)

        config = pass_opts + " " + comp_flag
        # do run time
        if bench == '400.perlbench':
            print "running..."
            sim_t = []
            or_t = []
            for i in range(0):
                start_1 = default_timer()
                subprocess.call("taskset 0x00000001 " + dir_hash + "/perlbench_sim -I./benchspec/CPU2006/400.perlbench/data/all/input/lib benchspec/CPU2006/400.perlbench/data/ref/input/checkspam.pl 2500 5 25 11 150 1 1 1 1 >perl_lol 2>perl_err",shell=True)   #>" +dir_hash + "perlbench.ref.checkspam1.out " + dir_hash + " 2> perlbench.ref.checkspam1.err", shell=True)
                duration_1 = default_timer() - start_1
                sim_t.append(duration_1)
                start_2 = default_timer()
                if comp_flag == "-Os":
                    subprocess.call("taskset 0x00000001 " + "benchspec/CPU2006/" + bench + "/run/build_base_amd64-m64-gcc42-nn.0000/" + name + "_Os"  + " -I./benchspec/CPU2006/400.perlbench/data/all/input/lib benchspec/CPU2006/400.perlbench/data/ref/input/checkspam.pl 2500 5 25 11 150 1 1 1 1 >perl_lol2 2>perl_err2",shell=True)
                elif comp_flag == "-O2":
                    subprocess.call("taskset 0x00000001 " + "benchspec/CPU2006/" + bench + "/run/build_base_amd64-m64-gcc42-nn.0000/" + name + "_O2"  + " -I./benchspec/CPU2006/400.perlbench/data/all/input/lib benchspec/CPU2006/400.perlbench/data/ref/input/checkspam.pl 2500 5 25 11 150 1 1 1 1 >perl_lol2 2>perl_err2",shell=True)
                duration_2 = default_timer() - start_2
                or_t.append(duration_2)
            print bench, "run time results for 10 runs:"
            print "mean:",np.mean(sim_t), "std:",np.std(sim_t)
            print "actual data:",sim_t
            print "mean:",np.mean(or_t),"std:", np.std(or_t)
            print "actual data:", or_t
            print "2 tailed t-test p value:",ttest_rel(np.array(sim_t), np.array(or_t))[1]
        elif bench == '429.mcf':
            print "running..."
            sim_t = []
            or_t = []
            for i in range(1):
                start_1 = default_timer()
                print "taskset 0x00000001 " + dir_hash + "/mcf_sim  benchspec/CPU2006/429.mcf/data/ref/input/inp.in 1>" + dir_hash + "/mcf.ref.out"+str(i)+ " 2>" + dir_hash + "/mcf.ref.err" + str(i) +" "
                subprocess.call("taskset 0x00000001 " + dir_hash + "/mcf_sim  benchspec/CPU2006/429.mcf/data/ref/input/inp.in 1>" + dir_hash + "/mcf.ref.out"+str(i)+ " 2>" + dir_hash + "/mcf.ref.err" + str(i) +" ",shell=True)
                duration_1 = default_timer() - start_1
                sim_t.append(duration_1)
                start_2 = default_timer()
                if comp_flag == "-Os":
                    subprocess.call("taskset 0x00000001 " + "benchspec/CPU2006/" + bench + "/run/build_base_amd64-m64-gcc42-nn.0000/" + name + "_Os" + " benchspec/CPU2006/429.mcf/data/ref/input/inp.in 1>" + dir_hash + "/mcf.ref.out_os"+str(i)+ " 2>" + dir_hash + "/mcf.ref.err_os" + str(i) +" ",shell=True)
                elif comp_flag == "-O2":
                    subprocess.call("taskset 0x00000001 " + "benchspec/CPU2006/" + bench + "/run/build_base_amd64-m64-gcc42-nn.0000/" + name + "_O2" + " benchspec/CPU2006/429.mcf/data/ref/input/inp.in 1>" + dir_hash + "/mcf.ref.out_os"+str(i)+ " 2>" + dir_hash + "/mcf.ref.err_os" + str(i) +" ",shell=True)
                duration_2 = default_timer() - start_2
                or_t.append(duration_2)
            print bench, "run time results for 10 runs:"
            print "mean:",np.mean(sim_t), "std:",np.std(sim_t)
            print "actual data:",sim_t
            print "mean:",np.mean(or_t),"std:", np.std(or_t)
            print "actual data:", or_t
            print "2 tailed t-test p value:",ttest_rel(np.array(sim_t), np.array(or_t))[1]
        elif bench == '433.milc':
            print "running..."
            sim_t = []
            or_t = []
            for i in range(1):
                start_1 = default_timer()
                print "start"
                print "taskset 0x00000001 " + dir_hash + "/milc_sim < benchspec/CPU2006/433.milc/data/ref/input/su3imp.in 1>" + dir_hash + "/milc.ref.out"+str(i)+ " 2>" + dir_hash + "/milc.ref.err" + str(i) +" "
                subprocess.call("taskset 0x00000001 " + dir_hash + "/milc_sim  < benchspec/CPU2006/433.milc/data/ref/input/su3imp.in 1>" + dir_hash + "/milc.ref.out"+str(i)+ " 2>" + dir_hash + "/milc.ref.err" + str(i) +" ",shell=True)
                duration_1 = default_timer() - start_1
                print duration_1
                sim_t.append(duration_1)
                start_2 = default_timer()
                if comp_flag == "-Os":
                    subprocess.call("taskset 0x00000001 " + "benchspec/CPU2006/" + bench + "/run/build_base_amd64-m64-gcc42-nn.0000/" + name + "_Os" + " < benchspec/CPU2006/433.milc/data/ref/input/su3imp.in 1>" + dir_hash + "/milc.ref.out_os"+str(i)+ " 2>" + dir_hash + "/milc.ref.err_os" + str(i) +" ",shell=True)
                elif comp_flag == "-O2":
                    subprocess.call("taskset 0x00000001 " + "benchspec/CPU2006/" + bench + "/run/build_base_amd64-m64-gcc42-nn.0000/" + name + "_O2" + " < benchspec/CPU2006/433.milc/data/ref/input/su3imp.in 1>" + dir_hash + "/milc.ref.out_os"+str(i)+ " 2>" + dir_hash + "/milc.ref.err_os" + str(i) +" ",shell=True)
                duration_2 = default_timer() - start_2
                or_t.append(duration_2)
            print bench, "run time results for 10 runs:"
            print "mean:",np.mean(sim_t), "std:",np.std(sim_t)
            print "actual data:",sim_t
            print "mean:",np.mean(or_t),"std:", np.std(or_t)
            print "actual data:", or_t
            print "2 tailed t-test p value:",ttest_rel(np.array(sim_t), np.array(or_t))[1]
        elif bench == '450.soplex':
            print "running..."
            sim_t = []
            or_t = []
            for i in range(1): #13
                start_1 = default_timer()
                subprocess.call("taskset 0x00000001 " + dir_hash + "/soplex_sim -s1 -e -m45000 pds-50.mps  1>" + dir_hash + "/soplex.ref.out"+str(i)+ " 2>" + dir_hash + "/soplex.ref.err" + str(i) +" ",shell=True)
                duration_1 = default_timer() - start_1
                print duration_1
                sim_t.append(duration_1)
                start_2 = default_timer()
                if comp_flag == "-Os":
                    subprocess.call("taskset 0x00000001 " + "benchspec/CPU2006/" + bench + "/run/build_base_amd64-m64-gcc42-nn.0000/" + name + "_Os" + " -s1 -e -m45000 pds-50.mps 1>" + dir_hash + "/soplex.ref.out_os"+str(i)+ " 2>" + dir_hash + "/soplex.ref.err_os" + str(i) +" ",shell=True)
                elif comp_flag == "-O2":
                    subprocess.call("taskset 0x00000001 " + "benchspec/CPU2006/" + bench + "/run/build_base_amd64-m64-gcc42-nn.0000/" + name + "_O2" + " -s1 -e -m45000 pds-50.mps 1>" + dir_hash + "/soplex.ref.out_os"+str(i)+ " 2>" + dir_hash + "/soplex.ref.err_os" + str(i) +" ",shell=True)
                duration_2 = default_timer() - start_2
                or_t.append(duration_2)
            print bench, "run time results for 10 runs:"
            print "mean:",np.mean(sim_t), "std:",np.std(sim_t)
            print "actual data:",sim_t
            print "mean:",np.mean(or_t),"std:", np.std(or_t)
            print "actual data:", or_t
            print "2 tailed t-test p value:",ttest_rel(np.array(sim_t), np.array(or_t))[1]
        elif bench == '453.povray':
            print "running..."
            sim_t = []
            or_t = []
            for i in range(1): #40
                subprocess.call("cp benchspec/CPU2006/453.povray/data/ref/input/* " + dir_hash ,shell=True)
                start_1 = default_timer()
                print dir_hash + "/povray_sim benchspec/CPU2006/453.povray/data/ref/input/SPEC-benchmark-ref.ini"
                subprocess.call("taskset 0x00000001 " + dir_hash + "/povray_sim benchspec/CPU2006/453.povray/data/ref/input/SPEC-benchmark-ref.ini  1>" + dir_hash + "/povray.ref.out"+str(i)+ " 2>" + dir_hash + "/povray.ref.err" + str(i) +" ",shell=True)
                duration_1 = default_timer() - start_1
                sim_t.append(duration_1)
                start_2 = default_timer()
                if comp_flag == "-Os":
                    subprocess.call("taskset 0x00000001 " + "benchspec/CPU2006/" + bench + "/run/build_base_amd64-m64-gcc42-nn.0000/" + name + "_Os" + " benchspec/CPU2006/453.povray/data/ref/input/SPEC-benchmark-ref.ini 1>" + dir_hash + "/povray.ref.out_os"+str(i)+ " 2>" + dir_hash + "/povray.ref.err_os" + str(i) +" ",shell=True)
                elif comp_flag == "-O2":
                    subprocess.call("taskset 0x00000001 " + "benchspec/CPU2006/" + bench + "/run/build_base_amd64-m64-gcc42-nn.0000/" + name + "_O2" + " benchspec/CPU2006/453.povray/data/ref/input/SPEC-benchmark-ref.ini 1>" + dir_hash + "/povray.ref.out_os"+str(i)+ " 2>" + dir_hash + "/povray.ref.err_os" + str(i) +" ",shell=True)
                duration_2 = default_timer() - start_2
                or_t.append(duration_2)
            print bench, "run time results for 10 runs:"
            print "mean:",np.mean(sim_t), "std:",np.std(sim_t)
            print "actual data:",sim_t
            print "mean:",np.mean(or_t),"std:", np.std(or_t)
            print "actual data:", or_t
            print "2 tailed t-test p value:",ttest_rel(np.array(sim_t), np.array(or_t))[1]
        elif bench == '456.hmmer':
            print "running..."
            sim_t = []
            or_t = []
            for i in range(1): # 40
                start_1 = default_timer()
                #print dir_hash + "/hmmer_sim benchspec/CPU2006/456.hmmer/data/ref/input/nph3.hmm swiss41"
                subprocess.call("taskset 0x00000001 " + dir_hash + "/hmmer_sim benchspec/CPU2006/456.hmmer/data/ref/input/nph3.hmm benchspec/CPU2006/456.hmmer/data/ref/input/swiss41  1>" + dir_hash + "/hmmer.ref.out"+str(i)+ " 2>" + dir_hash + "/hmmer.ref.err" + str(i) +" ",shell=True)
                duration_1 = default_timer() - start_1
                sim_t.append(duration_1)
                start_2 = default_timer()
                if comp_flag == "-Os":
                    subprocess.call("taskset 0x00000001 " + "benchspec/CPU2006/" + bench + "/run/build_base_amd64-m64-gcc42-nn.0000/" + name + "_Os" + " benchspec/CPU2006/456.hmmer/data/ref/input/nph3.hmm benchspec/CPU2006/456.hmmer/data/ref/input/swiss41 1>" + dir_hash + "/hmmer.ref.out_os"+str(i)+ " 2>" + dir_hash + "/hmmer.ref.err_os" + str(i) +" ",shell=True)
                elif comp_flag == "-O2":
                    subprocess.call("taskset 0x00000001 " + "benchspec/CPU2006/" + bench + "/run/build_base_amd64-m64-gcc42-nn.0000/" + name + "_O2" + " benchspec/CPU2006/456.hmmer/data/ref/input/nph3.hmm benchspec/CPU2006/456.hmmer/data/ref/input/swiss41 1>" + dir_hash + "/hmmer.ref.out_os"+str(i)+ " 2>" + dir_hash + "/hmmer.ref.err_os" + str(i) +" ",shell=True)
                duration_2 = default_timer() - start_2
                or_t.append(duration_2)
            print bench, "run time results for 10 runs:"
            print "mean:",np.mean(sim_t), "std:",np.std(sim_t)
            print "actual data:",sim_t
            print "mean:",np.mean(or_t),"std:", np.std(or_t)
            print "actual data:", or_t
            print "2 tailed t-test p value:",ttest_rel(np.array(sim_t), np.array(or_t))[1]
        elif bench == '458.sjeng':
            print "running..."
            sim_t = []
            or_t = []
            for i in range(1):
                start_1 = default_timer()
                subprocess.call("taskset 0x00000001 " + dir_hash + "/hmmer_sim benchspec/CPU2006/458.sjeng/data/ref/input/ref.txt 1>" + dir_hash + "/sjeng.ref.out"+str(i)+ " 2>" + dir_hash + "/sjeng.ref.err" + str(i) +" ",shell=True)
                duration_1 = default_timer() - start_1
                sim_t.append(duration_1)
                start_2 = default_timer()
                if comp_flag == "-Os":
                    subprocess.call("taskset 0x00000001 " + "benchspec/CPU2006/" + bench + "/run/build_base_amd64-m64-gcc42-nn.0000/" + name + "_Os" + " benchspec/CPU2006/458.sjeng/data/ref/input/ref.txt 1>" + dir_hash + "/sjeng.ref.out_os"+str(i)+ " 2>" + dir_hash + "/sjeng.ref.err_os" + str(i) +" ",shell=True)
                elif comp_flag == "-O2":
                    subprocess.call("taskset 0x00000001 " + "benchspec/CPU2006/" + bench + "/run/build_base_amd64-m64-gcc42-nn.0000/" + name + "_O2" + " benchspec/CPU2006/458.sjeng/data/ref/input/ref.txt 1>" + dir_hash + "/sjeng.ref.out_os"+str(i)+ " 2>" + dir_hash + "/sjeng.ref.err_os" + str(i) +" ",shell=True)
                duration_2 = default_timer() - start_2
                or_t.append(duration_2)
            print bench, "run time results for 10 runs:"
            print "mean:",np.mean(sim_t), "std:",np.std(sim_t)
            print "actual data:",sim_t
            print "mean:",np.mean(or_t),"std:", np.std(or_t)
            print "actual data:", or_t
            print "2 tailed t-test p value:",ttest_rel(np.array(sim_t), np.array(or_t))[1]
        elif bench == '462.libquantum':
            print "running..."
            sim_t = []
            or_t = []
            for i in range(1):
                start_1 = default_timer()
                subprocess.call("taskset 0x00000001 " + dir_hash + "/libquantum_sim 1397 8 1>" + dir_hash + "/libquantum.ref.out"+str(i)+ " 2>" + dir_hash + "/sjeng.ref.err" + str(i) +" ",shell=True)
                duration_1 = default_timer() - start_1
                sim_t.append(duration_1)
                start_2 = default_timer()
                if comp_flag == "-Os":
                    subprocess.call("taskset 0x00000001 " + "benchspec/CPU2006/" + bench + "/run/build_base_amd64-m64-gcc42-nn.0000/" + name + "_Os" + " 1397 8 1>" + dir_hash + "/libquantum.ref.out_os"+str(i)+ " 2>" + dir_hash + "/libquantum.ref.err_os" + str(i) +" ",shell=True)
                elif comp_flag == "-O2":
                    subprocess.call("taskset 0x00000001 " + "benchspec/CPU2006/" + bench + "/run/build_base_amd64-m64-gcc42-nn.0000/" + name + "_O2" + " 1397 8 1>" + dir_hash + "/libquantum.ref.out_os"+str(i)+ " 2>" + dir_hash + "/libquantum.ref.err_os" + str(i) +" ",shell=True)
                duration_2 = default_timer() - start_2
                or_t.append(duration_2)
            print bench, "run time results for 10 runs:"
            print "mean:",np.mean(sim_t), "std:",np.std(sim_t)
            print "actual data:",sim_t
            print "mean:",np.mean(or_t),"std:", np.std(or_t)
            print "actual data:", or_t
            print "2 tailed t-test p value:",ttest_rel(np.array(sim_t), np.array(or_t))[1]
        elif bench == '464.h264ref':
            print "running..."
            sim_t = []
            or_t = []
            for i in range(1):
                #./data/all/input/
                print dir_hash + "/h264ref_sim -d benchspec/CPU2006/464.h264ref/data/ref/input/foreman_ref_encoder_baseline.cfg"
                start_1 = default_timer()
                subprocess.call("taskset 0x00000001 " + dir_hash + "/h264ref_sim -d benchspec/CPU2006/464.h264ref/data/ref/input/foreman_ref_encoder_baseline.cfg 1>" + dir_hash + "/h264ref.ref.out"+str(i)+ " 2>" + dir_hash + "/h264ref.ref.err" + str(i) +" ",shell=True)
                duration_1 = default_timer() - start_1
                sim_t.append(duration_1)
                start_2 = default_timer()
                if comp_flag == "-Os":
                    subprocess.call("taskset 0x00000001 " + "benchspec/CPU2006/" + bench + "/run/build_base_amd64-m64-gcc42-nn.0000/" + name + "_Os" + " -d  benchspec/CPU2006/464.h264ref/data/ref/input/foreman_ref_encoder_baseline.cfg 1>" + dir_hash + "/h264ref.ref.out_os"+str(i)+ " 2>" + dir_hash + "/h264ref.ref.err_os" + str(i) +" ",shell=True)
                elif comp_flag == "-O2":
                    subprocess.call("taskset 0x00000001 " + "benchspec/CPU2006/" + bench + "/run/build_base_amd64-m64-gcc42-nn.0000/" + name + "_O2" + " -d  benchspec/CPU2006/464.h264ref/data/ref/input/ foreman_ref_encoder_baseline.cfg 1>" + dir_hash + "/h264ref.ref.out_os"+str(i)+ " 2>" + dir_hash + "/h274ref.ref.err_os" + str(i) +" ",shell=True)
                duration_2 = default_timer() - start_2
                or_t.append(duration_2)
            print bench, "run time results for 10 runs:"
            print "mean:",np.mean(sim_t), "std:",np.std(sim_t)
            print "actual data:",sim_t
            print "mean:",np.mean(or_t),"std:", np.std(or_t)
            print "actual data:", or_t
            print "2 tailed t-test p value:",ttest_rel(np.array(sim_t), np.array(or_t))[1]
        elif bench == '471.omnetpp':
            print "running..."
            sim_t = []
            or_t = []
            for i in range(1):
                print dir_hash + "/omnetpp_sim  benchspec/CPU2006/471.omnetpp/data/ref/input/omnetpp.ini 1>" + dir_hash + "/omnetpp.ref.out"+str(i)+ " 2>" + dir_hash + "/omnetpp.ref.err" + str(i) +" "
                start_1 = default_timer()
                subprocess.call("taskset 0x00000001 " + dir_hash + "/omnetpp_sim  benchspec/CPU2006/471.omnetpp/data/ref/input/omnetpp.ini 1>" + dir_hash + "/omnetpp.ref.out"+str(i)+ " 2>" + dir_hash + "/omnetpp.ref.err" + str(i) +" ",shell=True)
                duration_1 = default_timer() - start_1
                sim_t.append(duration_1)
                start_2 = default_timer()
                if comp_flag == "-Os":
                    subprocess.call("taskset 0x00000001 " + "benchspec/CPU2006/" + bench + "/run/build_base_amd64-m64-gcc42-nn.0000/" + name + "_Os" + " benchspec/CPU2006/471.omnetpp/data/ref/input/omnetpp.ini 1>" + dir_hash + "/omnetpp.ref.out_os"+str(i)+ " 2>" + dir_hash + "/omnetpp.ref.err_os" + str(i) +" ",shell=True)
                elif comp_flag == "-O2":
                    subprocess.call("taskset 0x00000001 " + "benchspec/CPU2006/" + bench + "/run/build_base_amd64-m64-gcc42-nn.0000/" + name + "_O2" + " benchspec/CPU2006/471.omnetpp/data/ref/input/omnetpp.ini 1>" + dir_hash + "/omnetpp.ref.out_os"+str(i)+ " 2>" + dir_hash + "/omnetpp.ref.err_os" + str(i) +" ",shell=True)
                duration_2 = default_timer() - start_2
                or_t.append(duration_2)
            print bench, "run time results for 10 runs:"
            print "mean:",np.mean(sim_t), "std:",np.std(sim_t)
            print "actual data:",sim_t
            print "mean:",np.mean(or_t),"std:", np.std(or_t)
            print "actual data:", or_t
            print "2 tailed t-test p value:",ttest_rel(np.array(sim_t), np.array(or_t))[1]
    except Exception as e:
        print "ERROR run failed unexpectedly, aborting..." ,bench, pass_opts, comp_flag, "REASON: ", e
    finally:
        print "done"
def unzip(l):
    return (zip(*l))

if __name__ == '__main__':
    name = sys.argv[1]
    print name
    execfile(name)
    for x,y in best_d.items():
        if x == '471.omnetpp':
            run_bench((x,y[0]))
# gcc 4.6 does not add the shared lib to the executable if no symbols from it are referenced. This is a deviation from earlier gcc behavior (they always added the shared library).

# gcc 4.6 adds the --as-needed option to the linker (ld) command line so we need to turn that off.

# Fix: add --no-as-needed in front of the library 
