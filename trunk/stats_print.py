import subprocess
from multiprocessing import Process, Pool

cpu_int = ['400.perlbench','401.bzip2', '403.gcc', '429.mcf', '445.gobmk', '456.hmmer', '458.sjeng', '462.libquantum', '464.h264ref', '471.omnetpp','473.astar','483.xalancbmk']
cpu_fp = ['433.milc','444.namd','447.dealll','450.soplex','453.povray','470.lbm','482.sphinx3']


# for bench in cpu_int + cpu_fp:
#     name = bench.split('.')[1]
#     file_str = "benchspec/CPU2006/" + bench + "/run/build_base_amd64-m64-gcc42-nn.0000/" + name + "_ind_ls.bc "
#     b = subprocess.call("opt  -load ../llvm-3.5/Debug+Asserts/lib/LoopStats.so -loopstat -bench="+name+ " " + file_str + " 1>/dev/null", shell=True)
#     print "#",b

for bench in cpu_int + cpu_fp:
    name = bench.split('.')[1]
    print "#",name
    file_str = "benchspec/CPU2006/" + bench + "/run/build_base_amd64-m64-gcc42-nn.0000/" + name + "_ind_ls.bc "
    b = subprocess.call("../llvm-3.5/Debug+Asserts/bin/opt  -load ../llvm-3.5/Debug+Asserts/lib/SimMerge.so -simmergefunc --max-num-bbs=0 " + file_str + " 1>/dev/null", shell=True)
