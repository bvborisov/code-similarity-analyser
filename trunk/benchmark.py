import subprocess
from multiprocessing import Process, Pool

cpu_int = ['400.perlbench','401.bzip2', '403.gcc', '429.mcf', '445.gobmk', '456.hmmer', '458.sjeng', '462.libquantum', '464.h264ref', '471.omnetpp','473.astar']#'483.xalancbmk']
cpu_fp = ['433.milc','444.namd','447.dealII','450.soplex','453.povray','470.lbm','482.sphinx3']
# source 
print "HAVE YOU SOURCED?"

##############################

print "Compiling benchmarks to bc"
def compile_bench(bench):
    print bench
    name = bench.split('.')[1]
    subprocess.call("runspec --config=linux64-amd64-clang3.5.cfg --action=build --tune=base " + name + " 2>/dev/null", shell=True)
    print "runspec --config=linux64-amd64-clang3.5.cfg --action=build --tune=base " + name + " 2>/dev/null"
    print "linking..."  
    a = subprocess.call("llvm-link -o benchspec/CPU2006/" + bench + "/run/build_base_amd64-m64-gcc42-nn.0000/" + name +".bc `find benchspec/CPU2006/" + bench + "/run/build_base_amd64-m64-gcc42-nn.0000/ -name \"*.o\"`", shell=True)
    print a
    print "indvars_loop_simplify..."
    # -licm ? -instcombine?
    b = subprocess.call("opt  -indvars  -loop-simplify  -verify benchspec/CPU2006/" + bench + "/run/build_base_amd64-m64-gcc42-nn.0000/" + name + ".bc  1>benchspec/CPU2006/" + bench + "/run/build_base_amd64-m64-gcc42-nn.0000/" + name + "_ind_ls.bc", shell=True)
    c = subprocess.call("opt -simplifycfg -verify benchspec/CPU2006/" + bench + "/run/build_base_amd64-m64-gcc42-nn.0000/" + name + "_ind_ls.bc 1>" + "benchspec/CPU2006/" + bench + "/run/build_base_amd64-m64-gcc42-nn.0000/" + name +"_cfg.bc ", shell=True)
    extra = ''
    if name == "perlbench":
        extra =  " -DSPEC_CPU_LP64 -DSPEC_CPU_LINUX_X64 -lm"
    elif name == "libquantum":
        extra =  " -lm"
    elif name == "gobmk":
        extra = " -lm"
    elif name == "hmmer":
        extra = " -lm"
    elif name == "h264ref":
        extra = " -lm"
    elif name == "milc":
        extra = " -DSPEC_CPU_LP64 -lm"
    elif name == "soplex": 
        extra = " -lm"
    elif name == "sphinx3":
        extra = " -lm"
    elif name == "lbm":
        extra = " -lm"
    elif name == "gcc":
        extra = " -lm -DSPEC_CPU_LP64"
        # set compiler
    comp = " clang "
    if name == "omnetpp" or name == "astar" or name == "namd" or name == "soplex" or name == "povray":
        comp = "clang++ "
    d = subprocess.call(comp + " -Os" + " -o " + "benchspec/CPU2006/" + bench + "/run/build_base_amd64-m64-gcc42-nn.0000/" + name + "_Os " + "benchspec/CPU2006/" + bench + "/run/build_base_amd64-m64-gcc42-nn.0000/" + name + "_cfg.bc" + extra, shell=True)
    e = subprocess.call(comp + " -O2" + " -o " + "benchspec/CPU2006/" + bench + "/run/build_base_amd64-m64-gcc42-nn.0000/" + name + "_O2 " + "benchspec/CPU2006/" + bench + "/run/build_base_amd64-m64-gcc42-nn.0000/" + name + "_cfg.bc" + extra, shell=True)
    subprocess.call("strip -s " + "benchspec/CPU2006/" + bench + "/run/build_base_amd64-m64-gcc42-nn.0000/" + name + "_Os", shell=True)
    subprocess.call("strip -s " + "benchspec/CPU2006/" + bench + "/run/build_base_amd64-m64-gcc42-nn.0000/" + name + "_O2", shell=True)
    print bench, b,c,d,e

############################

############################



if __name__ == '__main__':
    pool = Pool(processes=16)
    args = cpu_int + cpu_fp
    # prepare list of pairs - benches by all possible comibations of input arguments
    # also add option for different compiler flags
    # also add stuff from bench3 and 4 and finally check file size and output all the stuff to a file in a results_dir
    # by date
    pool.map(compile_bench,args)
    # do big link
    in_str = ''
    for bench in cpu_int + cpu_fp:
        name = bench.split('.')[1]
        in_str += "benchspec/CPU2006/" + bench + "/run/build_base_amd64-m64-gcc42-nn.0000/" + name + "_ind_ls.bc "


