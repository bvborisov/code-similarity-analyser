import subprocess
import time
import itertools
import os
import md5
from multiprocessing import Pool, Process, Manager, Lock
import matplotlib.pyplot as plt
import numpy as np
import sys
from random import shuffle

manager = Manager()
# best flag per bench
lproxy = manager.list()
lproxy.append({})
lproxy.append({})
build = 'Release+Asserts'
# best config on avg

# global lock
lock = Lock()

cpu_int = ['400.perlbench','401.bzip2', '403.gcc', '429.mcf', '445.gobmk', '456.hmmer', '458.sjeng', '462.libquantum', '464.h264ref', '471.omnetpp','473.astar']   #,'483.xalancbmk']
cpu_fp = ['433.milc','444.namd','447.dealll','450.soplex','453.povray','470.lbm','482.sphinx3']
#best_cands = ['462.libquantum','464.h264ref','433.milc','400.perlbench','401.bzip2']
# parameters
min_sim = ["0.8","0.9"]
min_insts = ["0"]
max_insts = ["200","300","400"]
min_common_values = ["0"]
max_common_values = ["6","10","20"]
min_num_bbs = ["0"]
max_num_bbs = ["10","23","30"]
sim_metric = ["0","1"]
use_heap = ["0","1"]
opts = ["-O2","-Os"]
def fix_params(l,s):
    for i in range(len(l)):
        l[i] = s + l[i]
fix_params(min_sim,"--min-sim=")
fix_params(min_insts, "--min-insts=")
fix_params(max_insts, "--max-insts=")
fix_params(min_common_values, "--min-num-common-values=")
fix_params(max_common_values, "--max-num-common-values=")
fix_params(min_num_bbs, "--min-num-bbs=")
fix_params(max_num_bbs, "--max-num-bbs=")
fix_params(sim_metric, "--sim-metric=")
fix_params(use_heap, "--use-heap=")

all_params = [min_sim, min_insts, max_insts, min_common_values, max_common_values, min_num_bbs, max_num_bbs, sim_metric, use_heap, opts]
all_settings = list(itertools.product(*all_params))

all_pairs = list(itertools.product(*[cpu_int+ cpu_fp, all_settings]))
#all_pairs = list(itertools.product(*[best_cands, all_settings]))
# source 
print "HAVE YOU SOURCED?"

num_failed = 0
def run_bench((bench,opts)):
    # set nice
    # niceness = os.nice(0)
    # os.nice(niceness - 10)
    # unpack
    #print d
    comp_flag = opts[-1]
    pass_opts = " ".join(opts[:-1])
    print bench
    name = bench.split('.')[1]

    print bench, pass_opts, comp_flag
    try:
        # run opt pass
        dir_str =  bench + pass_opts.replace(' ','_') + comp_flag
        dir_hash = "loop_merge_results10/" + md5.new(dir_str).hexdigest()
        direc = subprocess.call("mkdir -v " + dir_hash, shell=True);
        empty_dir = []
        try:
            empty_dir = os.listdir(dir_hash)
        except:
            pass
        if direc != 0 and "improvement" in empty_dir :
            print empty_dir
            print "Dir exists or is full, move on."
            return
        b = subprocess.call("../llvm-3.5/" + build + "/bin/opt -load ../llvm-3.5/" + build + "/lib/SimMerge.so -simmergefunc " + pass_opts +  " -verify benchspec/CPU2006/" + bench + "/run/build_base_amd64-m64-gcc42-nn.0000/" + name + "_ind_ls.bc  1>"+ dir_hash +"/" + name + "_sim.bc 2>" + dir_hash + "/" + name +".log", shell=True)
        if b != 0:
            print "pass failed, move on"
            subprocess.call('echo "pass failed, move on" && tail ' + dir_hash + "/" + name +".log",shell=True)
            subprocess.call("rm " + dir_hash +"/*", shell=True)
            return 
        # run simplifycfg on top
        b = subprocess.call("opt -simplifycfg -verify " + dir_hash + "/" + name + "_sim.bc 1>" + dir_hash +"/" + name + "_sim_cfg.bc ", shell=True)
        # compile executables
        extra = ''
        if name == "perlbench":
            extra =  " -DSPEC_CPU_LP64 -DSPEC_CPU_LINUX_X64 -lm"
        elif name == "libquantum":
            extra =  " -lm"
        elif name == "gobmk":
            extra = " -lm"
        elif name == "hmmer":
            extra = " -lm"
        elif name == "h264ref":
            extra = " -lm"
        elif name == "milc":
            extra = " -DSPEC_CPU_LP64 -lm"
        elif name == "soplex": 
            extra = " -lm"
        elif name == "sphinx3":
            extra = " -lm"
        elif name == "lbm":
            extra = " -lm"
        elif name == "gcc":
            extra = " -lm -DSPEC_CPU_LP64"
        # set compiler
        comp = " clang "
        if name == "omnetpp" or name == "astar" or name == "namd" or name == "soplex" or name == "povray":
            comp = "clang++ "
        subprocess.call(comp + comp_flag + " -o " + dir_hash + "/" + name + "_sim "  + " " + dir_hash + "/" + name+ "_sim_cfg.bc" + extra, shell=True)

        subprocess.call("strip -s " + dir_hash + "/" + name + "_sim", shell=True)
        orig_size = 0
        if comp_flag == "-Os":
            orig_size = os.path.getsize("benchspec/CPU2006/" + bench + "/run/build_base_amd64-m64-gcc42-nn.0000/" + name + "_Os");
        elif comp_flag == "-O2":
            orig_size = os.path.getsize("benchspec/CPU2006/" + bench + "/run/build_base_amd64-m64-gcc42-nn.0000/" + name + "_O2");
        sim_size = os.path.getsize(dir_hash + "/" + name +"_sim");
        impro = orig_size - sim_size
        print "RESULT FOR", bench, pass_opts, comp_flag, 1.0*sim_size/orig_size
        # f = open(dir_hash + "/improvement","w")
        # f.write(bench + " " + pass_opts + " " + comp_flag + "\n")
        # if impro > 0:
        #     f.write("pos:" + str(impro) + ", " + str(1.0*sim_size/orig_size) + "\n")
        # else:
        #     f.write("neg:" + str(impro) + ", " + str(1.0*sim_size/orig_size) + "\n")
        #     f.close()
        # when improvement is positive
        rel_impr = (1.0*sim_size)/(1.0*orig_size)
        config = pass_opts + " " + comp_flag
        # best flag per bench
        print "#########################"
        print bench, pass_opts,comp_flag, "wants to get the lock"
        lock.acquire()
        ds = lproxy[0]
        dr = lproxy[1]
        # what if we fail? keep number of successful run as well as sum!
        print bench, pass_opts,comp_flag, "acquired lock"
        if bench in ds:
            if ds[bench][1] > rel_impr:
                ds[bench][0] = config
                ds[bench][1] = rel_impr
            print bench, "in ds"
        else:
            ds[bench] = manager.list([config,rel_impr])
            print bench, "not in ds"

        # best config on average

        if config in dr:            
            dr[config] = (dr[config][0] + 1, dr[config][1] + rel_impr)
            print bench, "in ds"
        else:
            dr[config] = (1,rel_impr)
            print bench, "not in ds"
        lproxy[0] = ds
        lproxy[1] = dr
        lock.release()
        print bench, pass_opts,comp_flag, "released the lock"
        print "#########################"
        #     # delete files if improvement 0 or less -no need to run time comparison then?
        # else : benchmark execution...
    except Exception as e:
        print "ERROR run failed unexpectedly, aborting..." ,bench, pass_opts, comp_flag, "REASON: ", e
    finally:
        rm_stat = subprocess.call("rm -rf " + dir_hash, shell=True)
        print "rm -rf " + dir_hash, rm_stat

def show_scatter(xlabel, ylabel,xs,ys,c,name):
    xs = xs + np.random.normal(0,0.5,size=len(xs))
    ys = ys + np.random.normal(0,0.5,size=len(ys))
    fig, ax = plt.subplots()
    ax.set_aspect('equal')
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.set_xlim(0,max(np.max(xs),np.max(ys)))
    ax.set_ylim(0,max(np.max(xs),np.max(ys)))
    ax.scatter(xs,ys,c=c)
    ax.plot([0,max(np.max(xs),np.max(ys))],[0,max(np.max(xs),np.max(ys))],'k-',alpha=0.75, zorder=0)
    plt.savefig(name, bbox_inches='tight')
    plt.close()
    #plt.show()

def unzip(l):
    return (zip(*l))

if __name__ == '__main__':
    procs = int(sys.argv[1])
    name = sys.argv[2]
    pool = Pool(processes=procs)
    pairs = []
    # prepare list of pairs - benches by all possible comibations of input arguments
    # also add option for different compiler flags
    # also add stuff from bench3 and 4 and finally check file size and output all the stuff to a file in a results_dir
    # by date
    # ensure they finish at the same time
    shuffle(all_pairs)
    pool.map(run_bench,all_pairs,len(all_pairs)/procs)
    print lproxy[0]
    print lproxy[1]

    f = open('config_results' + name + '.py','w')
    f.write("best_d="+str(lproxy[0])+"\n")
    f.write("avg_d="+str(lproxy[1])+"\n")
    f.close()

