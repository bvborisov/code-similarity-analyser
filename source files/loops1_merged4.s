	.text
	.file	"source files/loops1_merged4.bc"
	.globl	main
	.align	16, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:                                 # %entry
	pushq	%rbp
.Ltmp0:
	.cfi_def_cfa_offset 16
.Ltmp1:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
.Ltmp2:
	.cfi_def_cfa_register %rbp
	subq	$96, %rsp
	movl	$0, -4(%rbp)
	movq	.Lmain.A+32(%rip), %rax
	movq	%rax, -16(%rbp)
	movaps	.Lmain.A+16(%rip), %xmm0
	movaps	%xmm0, -32(%rbp)
	movaps	.Lmain.A(%rip), %xmm0
	movaps	%xmm0, -48(%rbp)
	movq	.Lmain.B+16(%rip), %rax
	movq	%rax, -64(%rbp)
	movaps	.Lmain.B(%rip), %xmm0
	movaps	%xmm0, -80(%rbp)
	movl	.Lmain.C(%rip), %eax
	movl	%eax, -84(%rbp)
	movl	$0, -88(%rbp)
	leaq	-48(%rbp), %rdi
	leaq	-84(%rbp), %rdx
	leaq	-88(%rbp), %rcx
	movl	$0, %esi
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	callq	for.cond7_for.cond_merged
	movl	$123, -92(%rbp)
	movl	$246, -92(%rbp)
	movl	$0, -96(%rbp)
	leaq	-80(%rbp), %rsi
	leaq	-84(%rbp), %rdx
	leaq	-96(%rbp), %r8
	movl	$0, %edi
	xorl	%ecx, %ecx
	movl	$1, %r9d
	callq	for.cond7_for.cond_merged
	xorl	%eax, %eax
	addq	$96, %rsp
	popq	%rbp
	retq
.Ltmp3:
	.size	main, .Ltmp3-main
	.cfi_endproc

	.globl	for.cond7_for.cond_merged
	.align	16, 0x90
	.type	for.cond7_for.cond_merged,@function
for.cond7_for.cond_merged:              # @for.cond7_for.cond_merged
	.cfi_startproc
# BB#0:                                 # %for.cond7_for.cond_merged_header
	pushq	%rbp
.Ltmp4:
	.cfi_def_cfa_offset 16
.Ltmp5:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
.Ltmp6:
	.cfi_def_cfa_register %rbp
	jmp	.LBB1_1
	.align	16, 0x90
.LBB1_17:                               # %for.inc19
                                        #   in Loop: Header=BB1_1 Depth=1
	movl	(%rax), %eax
	addl	%eax, (%rdx)
	incl	(%r8)
.LBB1_1:                                # %for.cond7
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r8), %eax
	testb	$1, %r9b
	je	.LBB1_3
# BB#2:                                 # %extra_l_diff
                                        #   in Loop: Header=BB1_1 Depth=1
	cmpl	$6, %eax
	jmp	.LBB1_4
	.align	16, 0x90
.LBB1_3:                                # %extra_p_diff
                                        #   in Loop: Header=BB1_1 Depth=1
	cmpl	$10, %eax
.LBB1_4:                                # %for.cond7_bl
                                        #   in Loop: Header=BB1_1 Depth=1
	setl	%al
	testb	%al, %al
	je	.LBB1_18
# BB#5:                                 # %for.body9
                                        #   in Loop: Header=BB1_1 Depth=1
	movslq	(%r8), %rax
	testb	$1, %r9b
	je	.LBB1_7
# BB#6:                                 # %extra_l_diff1
                                        #   in Loop: Header=BB1_1 Depth=1
	leaq	(%rsi,%rax,4), %rax
	jmp	.LBB1_8
	.align	16, 0x90
.LBB1_7:                                # %extra_p_diff2
                                        #   in Loop: Header=BB1_1 Depth=1
	leaq	(%rdi,%rax,4), %rax
.LBB1_8:                                # %for.body9_bl
                                        #   in Loop: Header=BB1_1 Depth=1
	movl	(%rax), %r10d
	movslq	(%r8), %rax
	testb	$1, %r9b
	je	.LBB1_10
# BB#9:                                 # %extra_l_diff3
                                        #   in Loop: Header=BB1_1 Depth=1
	leaq	(%rsi,%rax,4), %rcx
	jmp	.LBB1_11
	.align	16, 0x90
.LBB1_10:                               # %extra_p_diff4
                                        #   in Loop: Header=BB1_1 Depth=1
	leaq	(%rdi,%rax,4), %rcx
.LBB1_11:                               # %for.body9_bl_bl
                                        #   in Loop: Header=BB1_1 Depth=1
	movl	(%rcx), %eax
	testb	$1, %r9b
	je	.LBB1_13
# BB#12:                                # %extra_l_diff5
                                        #   in Loop: Header=BB1_1 Depth=1
	addl	%r10d, %eax
	jmp	.LBB1_14
	.align	16, 0x90
.LBB1_13:                               # %extra_p_diff6
                                        #   in Loop: Header=BB1_1 Depth=1
	imull	%r10d, %eax
.LBB1_14:                               # %for.body9_bl_bl_bl
                                        #   in Loop: Header=BB1_1 Depth=1
	movl	%eax, (%rcx)
	movslq	(%r8), %rax
	testb	$1, %r9b
	je	.LBB1_16
# BB#15:                                # %extra_l_diff7
                                        #   in Loop: Header=BB1_1 Depth=1
	leaq	(%rsi,%rax,4), %rax
	jmp	.LBB1_17
	.align	16, 0x90
.LBB1_16:                               # %extra_p_diff8
                                        #   in Loop: Header=BB1_1 Depth=1
	leaq	(%rdi,%rax,4), %rax
	jmp	.LBB1_17
.LBB1_18:                               # %for.cond7_for.cond_merged_exit
	popq	%rbp
	retq
.Ltmp7:
	.size	for.cond7_for.cond_merged, .Ltmp7-for.cond7_for.cond_merged
	.cfi_endproc

	.type	.Lmain.A,@object        # @main.A
	.section	.rodata,"a",@progbits
	.align	16
.Lmain.A:
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	8                       # 0x8
	.long	9                       # 0x9
	.long	10                      # 0xa
	.size	.Lmain.A, 40

	.type	.Lmain.B,@object        # @main.B
	.align	16
.Lmain.B:
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	6                       # 0x6
	.size	.Lmain.B, 24

	.type	.Lmain.C,@object        # @main.C
	.section	.rodata.cst4,"aM",@progbits,4
	.align	4
.Lmain.C:
	.long	1                       # 0x1
	.size	.Lmain.C, 4


	.ident	"clang version 3.5 (trunk 201963)"
	.section	".note.GNU-stack","",@progbits
