; ModuleID = 'cfg.bc'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

; Function Attrs: nounwind uwtable
define void @_Z1fi(i32 %x) #0 {
entry:
  %x.addr = alloca i32, align 4
  %y = alloca i32, align 4
  %z = alloca i32, align 4
  store i32 %x, i32* %x.addr, align 4
  %0 = load i32* %x.addr, align 4
  %add = add nsw i32 %0, 2
  store i32 %add, i32* %y, align 4
  %1 = load i32* %y, align 4
  %cmp = icmp sgt i32 %1, 2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load i32* %y, align 4
  %add1 = add nsw i32 %2, 1
  store i32 %add1, i32* %y, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %3 = load i32* %y, align 4
  %mul = mul nsw i32 %3, 2
  store i32 %mul, i32* %z, align 4
  ret void
}

; Function Attrs: nounwind uwtable
define void @_Z1gi(i32 %x) #0 {
entry:
  %x.addr = alloca i32, align 4
  %y = alloca i32, align 4
  %z = alloca i32, align 4
  store i32 %x, i32* %x.addr, align 4
  %0 = load i32* %x.addr, align 4
  %add = add nsw i32 %0, 2
  store i32 %add, i32* %y, align 4
  %1 = load i32* %y, align 4
  %mul = mul nsw i32 %1, 2
  store i32 %mul, i32* %z, align 4
  ret void
}

attributes #0 = { nounwind uwtable "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.ident = !{!0}

!0 = metadata !{metadata !"clang version 3.5 (trunk 201963)"}
