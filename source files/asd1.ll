; ModuleID = 'source files/asd1.bc'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@main.A = private unnamed_addr constant [10 x i32] [i32 1, i32 2, i32 3, i32 4, i32 5, i32 6, i32 7, i32 8, i32 9, i32 10], align 16
@main.B = private unnamed_addr constant [6 x i32] [i32 1, i32 2, i32 3, i32 4, i32 5, i32 6], align 16
@main.C = private unnamed_addr constant [1 x i32] [i32 1], align 4

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
entry:
  %retval = alloca i32, align 4
  %A = alloca [10 x i32], align 16
  %B = alloca [6 x i32], align 16
  %C = alloca [1 x i32], align 4
  %i = alloca i32, align 4
  %x = alloca i32, align 4
  %j = alloca i32, align 4
  store i32 0, i32* %retval
  %0 = bitcast [10 x i32]* %A to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %0, i8* bitcast ([10 x i32]* @main.A to i8*), i64 40, i32 16, i1 false)
  %1 = bitcast [6 x i32]* %B to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %1, i8* bitcast ([6 x i32]* @main.B to i8*), i64 24, i32 16, i1 false)
  %2 = bitcast [1 x i32]* %C to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %2, i8* bitcast ([1 x i32]* @main.C to i8*), i64 4, i32 4, i1 false)
  store i32 0, i32* %i, align 4
  call void @for.cond7_for.cond_merged(i32* %i, i32* null, [10 x i32]* %A, [6 x i32]* null, [1 x i32]* %C, i1 false)
  br label %for.end

for.end:                                          ; preds = %entry
  store i32 123, i32* %x, align 4
  %3 = load i32* %x, align 4
  %4 = load i32* %x, align 4
  %add6 = add nsw i32 %4, %3
  store i32 %add6, i32* %x, align 4
  store i32 0, i32* %j, align 4
  call void @for.cond7_for.cond_merged(i32* null, i32* %j, [10 x i32]* null, [6 x i32]* %B, [1 x i32]* %C, i1 true)
  br label %for.end21

for.end21:                                        ; preds = %for.end
  ret i32 0
}

; Function Attrs: nounwind
declare void @llvm.memcpy.p0i8.p0i8.i64(i8* nocapture, i8* nocapture readonly, i64, i32, i1) #1

define void @for.cond7_for.cond_merged(i32* %i_m, i32* %j_m, [10 x i32]* %A_m, [6 x i32]* %B_m, [1 x i32]* %C_m, i1 %ctrl) {
for.cond7_for.cond_merged_header:
  br i1 %ctrl, label %for.cond7, label %for.cond

for.cond7:                                        ; preds = %for.inc19, %for.cond7_for.cond_merged_header
  %0 = load i32* %j_m, align 4
  %cmp8 = icmp slt i32 %0, 6
  br i1 %cmp8, label %for.body9, label %for.cond7_for.cond_merged_exit

for.body9:                                        ; preds = %for.cond7
  %1 = load i32* %j_m, align 4
  %idxprom10 = sext i32 %1 to i64
  %arrayidx11 = getelementptr inbounds [6 x i32]* %B_m, i32 0, i64 %idxprom10
  %2 = load i32* %arrayidx11, align 4
  %3 = load i32* %j_m, align 4
  %idxprom12 = sext i32 %3 to i64
  %arrayidx13 = getelementptr inbounds [6 x i32]* %B_m, i32 0, i64 %idxprom12
  %4 = load i32* %arrayidx13, align 4
  %add14 = add nsw i32 %4, %2
  store i32 %add14, i32* %arrayidx13, align 4
  %5 = load i32* %j_m, align 4
  %idxprom15 = sext i32 %5 to i64
  %arrayidx16 = getelementptr inbounds [6 x i32]* %B_m, i32 0, i64 %idxprom15
  %6 = load i32* %arrayidx16, align 4
  %arrayidx17 = getelementptr inbounds [1 x i32]* %C_m, i32 0, i64 0
  %7 = load i32* %arrayidx17, align 4
  %add18 = add nsw i32 %7, %6
  store i32 %add18, i32* %arrayidx17, align 4
  br label %for.inc19

for.inc19:                                        ; preds = %for.body9
  %8 = load i32* %j_m, align 4
  %inc20 = add nsw i32 %8, 1
  store i32 %inc20, i32* %j_m, align 4
  br label %for.cond7

for.cond:                                         ; preds = %for.inc, %for.cond7_for.cond_merged_header
  %9 = load i32* %i_m, align 4
  %cmp = icmp slt i32 %9, 10
  br i1 %cmp, label %for.body, label %for.cond7_for.cond_merged_exit

for.body:                                         ; preds = %for.cond
  %10 = load i32* %i_m, align 4
  %idxprom = sext i32 %10 to i64
  %arrayidx = getelementptr inbounds [10 x i32]* %A_m, i32 0, i64 %idxprom
  %11 = load i32* %arrayidx, align 4
  %12 = load i32* %i_m, align 4
  %idxprom1 = sext i32 %12 to i64
  %arrayidx2 = getelementptr inbounds [10 x i32]* %A_m, i32 0, i64 %idxprom1
  %13 = load i32* %arrayidx2, align 4
  %mul = mul nsw i32 %13, %11
  store i32 %mul, i32* %arrayidx2, align 4
  %14 = load i32* %i_m, align 4
  %idxprom3 = sext i32 %14 to i64
  %arrayidx4 = getelementptr inbounds [10 x i32]* %A_m, i32 0, i64 %idxprom3
  %15 = load i32* %arrayidx4, align 4
  %arrayidx5 = getelementptr inbounds [1 x i32]* %C_m, i32 0, i64 0
  %16 = load i32* %arrayidx5, align 4
  %add = add nsw i32 %16, %15
  store i32 %add, i32* %arrayidx5, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %17 = load i32* %i_m, align 4
  %inc = add nsw i32 %17, 1
  store i32 %inc, i32* %i_m, align 4
  br label %for.cond

for.cond7_for.cond_merged_exit:                   ; preds = %for.cond, %for.cond7
  ret void
}

attributes #0 = { nounwind uwtable "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind }

!llvm.ident = !{!0}

!0 = metadata !{metadata !"clang version 3.5 (trunk 201963)"}
