; ModuleID = 'source files/loops1.bc'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@main.A = private unnamed_addr constant [10 x i32] [i32 1, i32 2, i32 3, i32 4, i32 5, i32 6, i32 7, i32 8, i32 9, i32 10], align 16
@main.B = private unnamed_addr constant [6 x i32] [i32 1, i32 2, i32 3, i32 4, i32 5, i32 6], align 16
@main.C = private unnamed_addr constant [1 x i32] [i32 1], align 4

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
entry:
  %retval = alloca i32, align 4
  %A = alloca [10 x i32], align 16
  %B = alloca [6 x i32], align 16
  %C = alloca [1 x i32], align 4
  %i = alloca i32, align 4
  %x = alloca i32, align 4
  %j = alloca i32, align 4
  store i32 0, i32* %retval
  %0 = bitcast [10 x i32]* %A to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %0, i8* bitcast ([10 x i32]* @main.A to i8*), i64 40, i32 16, i1 false)
  %1 = bitcast [6 x i32]* %B to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %1, i8* bitcast ([6 x i32]* @main.B to i8*), i64 24, i32 16, i1 false)
  %2 = bitcast [1 x i32]* %C to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %2, i8* bitcast ([1 x i32]* @main.C to i8*), i64 4, i32 4, i1 false)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %3 = load i32* %i, align 4
  %cmp = icmp slt i32 %3, 10
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load i32* %i, align 4
  %idxprom = sext i32 %4 to i64
  %arrayidx = getelementptr inbounds [10 x i32]* %A, i32 0, i64 %idxprom
  %5 = load i32* %arrayidx, align 4
  %6 = load i32* %i, align 4
  %idxprom1 = sext i32 %6 to i64
  %arrayidx2 = getelementptr inbounds [10 x i32]* %A, i32 0, i64 %idxprom1
  %7 = load i32* %arrayidx2, align 4
  %mul = mul nsw i32 %7, %5
  store i32 %mul, i32* %arrayidx2, align 4
  %8 = load i32* %i, align 4
  %idxprom3 = sext i32 %8 to i64
  %arrayidx4 = getelementptr inbounds [10 x i32]* %A, i32 0, i64 %idxprom3
  %9 = load i32* %arrayidx4, align 4
  %arrayidx5 = getelementptr inbounds [1 x i32]* %C, i32 0, i64 0
  %10 = load i32* %arrayidx5, align 4
  %add = add nsw i32 %10, %9
  store i32 %add, i32* %arrayidx5, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32* %i, align 4
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 123, i32* %x, align 4
  %12 = load i32* %x, align 4
  %13 = load i32* %x, align 4
  %add6 = add nsw i32 %13, %12
  store i32 %add6, i32* %x, align 4
  store i32 0, i32* %j, align 4
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc19, %for.end
  %14 = load i32* %j, align 4
  %cmp8 = icmp slt i32 %14, 6
  br i1 %cmp8, label %for.body9, label %for.end21

for.body9:                                        ; preds = %for.cond7
  %15 = load i32* %j, align 4
  %idxprom10 = sext i32 %15 to i64
  %arrayidx11 = getelementptr inbounds [6 x i32]* %B, i32 0, i64 %idxprom10
  %16 = load i32* %arrayidx11, align 4
  %17 = load i32* %j, align 4
  %idxprom12 = sext i32 %17 to i64
  %arrayidx13 = getelementptr inbounds [6 x i32]* %B, i32 0, i64 %idxprom12
  %18 = load i32* %arrayidx13, align 4
  %add14 = add nsw i32 %18, %16
  store i32 %add14, i32* %arrayidx13, align 4
  %19 = load i32* %j, align 4
  %idxprom15 = sext i32 %19 to i64
  %arrayidx16 = getelementptr inbounds [6 x i32]* %B, i32 0, i64 %idxprom15
  %20 = load i32* %arrayidx16, align 4
  %arrayidx17 = getelementptr inbounds [1 x i32]* %C, i32 0, i64 0
  %21 = load i32* %arrayidx17, align 4
  %add18 = add nsw i32 %21, %20
  store i32 %add18, i32* %arrayidx17, align 4
  br label %for.inc19

for.inc19:                                        ; preds = %for.body9
  %22 = load i32* %j, align 4
  %inc20 = add nsw i32 %22, 1
  store i32 %inc20, i32* %j, align 4
  br label %for.cond7

for.end21:                                        ; preds = %for.cond7
  ret i32 0
}

; Function Attrs: nounwind
declare void @llvm.memcpy.p0i8.p0i8.i64(i8* nocapture, i8* nocapture readonly, i64, i32, i1) #1

attributes #0 = { nounwind uwtable "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind }

!llvm.ident = !{!0}

!0 = metadata !{metadata !"clang version 3.5 (trunk 201963)"}
